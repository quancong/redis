import com.redisbloomfilter.service.RedisBloomFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RedisBloomFilter.class)
public class XdclassMobileRedisApplicationTests {

	@Autowired
    RedisTemplate redisTemplate;
	@Test
	public void contextLoads() {

		redisTemplate.delete("myBloom");

	}

}

