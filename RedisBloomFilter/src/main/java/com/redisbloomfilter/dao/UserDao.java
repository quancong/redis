package com.redisbloomfilter.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface UserDao {

    @Select("select username from user where id = #{id}")
    public String getUserName(Integer id);
}
