package com.redisbloomfilter.controller;

import com.redisbloomfilter.service.RedisBloomFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RedisController {

    @Autowired
    private RedisBloomFilter redisBloomFilter;

    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/bloom/redisIdExists")
    public boolean redisidExists(int id){
        return redisBloomFilter.bloomFilterExists("myBloom",id);
    }

    @RequestMapping("/bloom/redisIdAdd")
    public boolean redisidAdd(int id){
        return redisBloomFilter.bloomFilterAdd("myBloom",id);
    }



}
