package com.redisbloomfilter.service;

import com.redisbloomfilter.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class CacheHitService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private UserDao userDao;

    /**
     * 分布式锁解决缓存击穿
     * @param key 键
     * @return
     */
    public String getKey(Integer key) {
        String value = (String)redisTemplate.opsForValue().get(key+"");
        if(value == null){
            String mutexKey = "mutex:key:"+key; //设置分布式锁的key
            if(redisTemplate.opsForValue().setIfAbsent(mutexKey,"1",180, TimeUnit.SECONDS)){ //给这个key上一把分布式锁，ex表示只有一个线程能执行，过期时间为180秒
                // 从数据库中查出该数据放入redis
                value = userDao.getUserName(key);
                redisTemplate.opsForValue().set(key+"",value);
                // 释放锁
                redisTemplate.delete(mutexKey);
            }else{
                // 其他的线程休息100毫秒后重试
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                getKey(key);
            }}
        return value;
    }
}
